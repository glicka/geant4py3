import geant4py as g4py
import numpy as np

def ej309_2in(name, copies=1, indices=[""]):
    '''
    2 inch EJ309 modules
    Used in MINER
    name will be appended with copy number
    '''

    assert copies >= 1
    thickness = 5. #mm
    window = 0.65 #mm
    #2 inches = 25.4*2 mm
    #2 inch radius = 25.4 mm

    dummy_dim = {"inner_rad": [0, 'mm'], "outer_rad": [25.4 + thickness + 1, 'mm'], "height": [25.4*2 + thickness + window + 1, 'mm'],'sPhi':[0,'deg'],'dPhi':[360,'deg']}
    dummy_primitive = g4py.geo.primitive.Cylinder('dummy', dummy_dim)
    dummy_material = g4py.geo.material.Vacuum()
    dummy_color = None

    # Setting up aluminum casing

    case_dim = {"inner_rad": [25.4, 'mm'], "outer_rad": [25.4 + thickness, 'mm'], "height": [25.4*2 + thickness + window, 'mm'],'sPhi':[0,'deg'],'dPhi':[360,'deg']}
    case_primitive = g4py.geo.primitive.Cylinder('case', case_dim)
    case_color = np.array([0.6, 0.6, 0.6, 0.5])
    case_material = g4py.geo.material.element("Al")

    # Setting up detector: sensitive region
    det_dim = {"inner_rad": [0, 'mm'], "outer_rad": [25.4, 'mm'], "height": [25.4*2, 'mm'],'sPhi':[0,'deg'],'dPhi':[360,'deg']}
    det_primitive = g4py.geo.primitive.Cylinder('detector', det_dim)
    det_material = g4py.geo.material.EJ309()
    det_color = [1, 1, 1, 1]


    # Creating and placing volumes
    vol = np.empty(copies, dtype=object)
    inner_air = np.empty(copies, dtype=object)
    detector = np.empty(copies, dtype=object)
    vol = np.empty(copies, dtype=object)

    for i in range(copies):
        dummy = g4py.geo.volume.Volume(name=name+"_"+str(i),
                                       primitive=dummy_primitive,
                                       material=dummy_material,
                                       sensitive=False,
                                       color=dummy_color,
                                       parent=None,
                                       translation=None,
                                       rotation=None)

        casing = g4py.geo.volume.Volume(name="case"+"_"+str(i),
                                        primitive=case_primitive,
                                        material=case_material,
                                        sensitive=False,
                                        color=case_color,
                                        parent=dummy,
                                        translation=None,
                                        rotation=None)

        detector = g4py.geo.volume.Volume(name='det'+"_"+str(i),
                                         primitive=det_primitive,
                                         material=det_material,
                                         sensitive=True,
                                         color=det_color,
                                         parent=dummy,
                                         translation=None,
                                         rotation=None)

        vol[i] = dummy
    if copies > 1: return vol
    else: return vol[0]
