import geant4py as g4py
import numpy as np
import math as m
import os

_HERE = os.path.dirname(os.path.abspath(__file__))

def b4cTest():



    # Setup dummy
    dummy_dim = {'inner_rad':[0,'mm'],
                                   'outer_rad':[5,'m'],
                                   'sPhi':[0,'deg'],
                                   'dPhi':[360,'deg'],
                                   'sTheta':[0,'deg'],
                                   'dTheta':[180,'deg']}
    dummy_color = [2, 2, 2, 2]
    dummy_primitive = g4py.geo.primitive.Sphere('dummySphere', dummy_dim)
    dummy_material = g4py.geo.material.Vacuum()

    dummy = g4py.geo.volume.Volume(name='dummy',
                                   primitive=dummy_primitive,
                                   material=dummy_material,
                                   color=None,
                                   sensitive=False,
                                   parent=None,
                                   translation=None,
                                   rotation=None)



    # Setup single crystal
    porcupinemodules = g4py.geo.prebuilt.b10_5in("5inDet", copies=1)
    #porcupinePMTs = g4py.geo.prebuilt.hamamatsuPMT("PMT", copies=1)
    #porcupineColls = g4py.geo.prebuilt.porcupineCollimator("Collimator", copies=1)
    porcupineShield = g4py.geo.prebuilt.b4csheet("Shield", copies=1)
    thickness = 5. #mm
    window = 0.65 #mm


    #det0 : 0 rotation
    #det1-7 : 30 deg rotation
    #det8-15: 60 deg rotation


    # Read in translations and rotations from file
    translations =  np.loadtxt(os.path.join(_HERE, "porcupine_positions1.txt"))
    rotations =  np.loadtxt(os.path.join(_HERE, "porcupine_rotations1.txt"))

    phi, theta = rotations
    #phi = np.deg2rad(phi)
    #theta = np.deg2rad(theta)
    #np.deg2rad(psi)
    #theta = np.arccos(translations[2]/100)
    #phi = m.atan2(translations[1],translations[0])
    r = [theta, phi]
    #r = [theta, phi, psi]#[np.sin(phi)*np.cos(theta), np.sin(phi)*np.sin(theta), 0]#np.cos(phi)]
    t = translations*10
    detH = 63.5*2 + thickness*2 + window
    pmtH = 139.7 + 25.4 + 63.5*2
    pmtDeltaZ = -detH/2 - pmtH/2
    #t[2] = t[2] + pmtDeltaZ

    porcupinemodules.placeit(parent=dummy,
                              translation=t,
                              rotation=r,
                              rotationName='rotAx')


    detH = 63.5*2 + thickness*2 + window

    '''
    collDeltaZ = (25.4*16 + 2)/2 - detH/2
    collDelta = [collDeltaZ*np.sin(phi)*np.cos(theta), collDeltaZ*np.sin(phi)*np.sin(theta), collDeltaZ*np.cos(phi)]
    tC = t + collDelta
    porcupineColls.placeit(parent=dummy,
                              translation=tC,
                          rotation=r,
                          rotationName='rotAx')#'HEP')
    '''
    '''
    detH = 63.5*2 + thickness*2 + window
    pmtH = 139.7 + 25.4 + 63.5*2
    pmtDeltaZ = -detH/2 - pmtH/2
    pmtDelta = [pmtDeltaZ*np.sin(phi)*np.cos(theta), pmtDeltaZ*np.sin(phi)*np.sin(theta), pmtDeltaZ*np.cos(phi)]
    t[2] += pmtDeltaZ
    r1 = [phi + np.deg2rad(180), theta]
    ## print 'translation = ', t

    porcupinePMTs.placeit(parent=dummy,
                              translation=t,#-(63.5*2 + thickness*2 + window + 1)],
                              rotation=r1,
                              rotationName='rotAx')

    t[2] -= pmtDeltaZ
    '''
    shieldDelta = detH/2 + (5 + 1)/2
    t[2] += shieldDelta

    porcupineShield.placeit(parent=dummy,
                              translation=t,#-(63.5*2 + thickness*2 + window + 1)],
                              rotation=None,
                              rotationName='rotAx')

    #t -= shieldDelta






    # Return
    return dummy
