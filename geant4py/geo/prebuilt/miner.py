import geant4py as g4py
import numpy as np
import os

_HERE = os.path.dirname(os.path.abspath(__file__))



def miner():



    # Setup dummy
    dummy_dim = {'inner_rad':[0,'mm'],
                 'outer_rad':[400,'mm'],
                 'height':[400,'mm'],
                 'sPhi':[0,'deg'],
                 'dPhi':[360,'deg']}
    dummy_primitive = g4py.geo.primitive.Cylinder('miner', dummy_dim)
    dummy_material = g4py.geo.material.Vacuum()
    dummy = g4py.geo.volume.Volume(name='miner',
                                   primitive=dummy_primitive,
                                   material=dummy_material,
                                   color=None,
                                   sensitive=False,
                                   parent=None,
                                   translation=None,
                                   rotation=None)

    # Setup single crystal
    '''
    Cylinder(name='cylinder', dim={'inner_rad':[0,'mm'],
                                       'outer_rad':[1,'mm'],
                                       'height':[1,'mm'],
                                       'sPhi':[0,'deg'],
                                       'dPhi':[360,'deg']})
    '''

    minermodules = g4py.geo.prebuilt.ej309_2in("detector", copies=16)




    # Read in translations and rotations from file
    translations =  np.loadtxt(os.path.join(_HERE, "miner_positions.txt"))

    # Loop and place detectors in world
    for i, trans in enumerate(translations):
        t = trans*10
        minermodules[i].placeit(parent=dummy,
                              translation=t,
                              rotation=None,
                              rotationName=None)

    # Return
    return dummy
