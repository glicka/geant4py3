import geant4py as g4py
import numpy as np

def porcupineLegCross(name, copies=1):
    '''
    80mm x 80mm x 500mm aluminum bar
    '''
    assert copies >= 1

    dummy_dim = {"x": [80 + 1, 'mm'], "y": [80 + 1, 'mm'], "z": [460, 'mm']}
    dummy_primitive = g4py.geo.primitive.BoxSolid('dummy', dummy_dim)
    dummy_material = g4py.geo.material.Vacuum()
    dummy_color = None

    casing_dim = {"x_start": [75.68 , 'mm'], "x_end": [80, 'mm'],
                  "y_start": [75.68 , 'mm'], "y_end": [80, 'mm'],
                  "z_start": [0, 'mm'], "z_end": [460, 'mm']}
    casing_primitive = g4py.geo.primitive.BoxHollow('casing', casing_dim)
    casing_material = g4py.geo.material.element("Al")
    casing_color = [1, 0, 0, 0.5]


    vol = np.empty(copies, dtype=object)
    for i in range(copies):
        dummy = g4py.geo.volume.Volume(name=name+"_"+str(i),
                                       primitive=dummy_primitive,
                                       material=dummy_material,
                                       sensitive=False,
                                       color=dummy_color,
                                       parent=None,
                                       translation=None,
                                       rotation=None)

        casing = g4py.geo.volume.Volume(name="cross"+"_"+str(i),
                                        primitive=casing_primitive,
                                        material=casing_material,
                                        sensitive=False,
                                        color=casing_color,
                                        parent=dummy,
                                        translation=None,
                                        rotation=None)



        vol[i] = dummy

    if copies > 1: return vol
    else: return vol[0]
