import geant4py as g4py
import numpy as np
import math as m
import os

_HERE = os.path.dirname(os.path.abspath(__file__))

def porcupineFrameWConcreteFloor():
    # Setup dummy
    dummy_dim = {'inner_rad':[0,'mm'],
                                   'outer_rad':[50,'m'],
                                   'sPhi':[0,'deg'],
                                   'dPhi':[360,'deg'],
                                   'sTheta':[0,'deg'],
                                   'dTheta':[180,'deg']}
    dummy_color = [2, 2, 2, 2]
    dummy_primitive = g4py.geo.primitive.Sphere('dummySphere', dummy_dim)
    dummy_material = g4py.geo.material.Vacuum()

    dummy = g4py.geo.volume.Volume(name='frameDummy',
                                   primitive=dummy_primitive,
                                   material=dummy_material,
                                   color=None,
                                   sensitive=False,
                                   parent=None,
                                   translation=None,
                                   rotation=None)



    # Setup single crystal
    lc=0
    legCross = g4py.geo.prebuilt.porcupineLegCross("cross", copies=4)
    ls=0
    legSide = g4py.geo.prebuilt.porcupineLegSide("side", copies=4)
    sp=0
    spine = g4py.geo.prebuilt.porcupineSpine("spine", copies=1)
    tn=0
    tenin = g4py.geo.prebuilt.porcupineArm19in("arm10", copies=3) #porcupineArm10in
    nn=0
    nineteenin = g4py.geo.prebuilt.porcupineArm19in("arm19", copies=4)
    tsn=0
    twentysixin = g4py.geo.prebuilt.porcupineArm26in("arm26", copies=4)
    ttn=0
    thirtytwoin = g4py.geo.prebuilt.porcupineArm32in("arm32", copies=5)
    b=0
    braces = g4py.geo.prebuilt.porcupineArm26in("brace", copies=16)



    # Get prebuilt Concrete Slab
    floor = g4py.geo.prebuilt.concreteFloor('floor', copies=1)



    porcupinemodules = g4py.geo.prebuilt.ej309_5in("5inDet", copies=16)
    porcupinePMTs = g4py.geo.prebuilt.hamamatsuPMT("PMT", copies=16)
    porcupineColls = g4py.geo.prebuilt.porcupineCollimatorCylinder("Collimator", copies=16)

    thickness = 5. #mm
    window = 0.65 #mm
    # Read in translations and rotations from file
    translations =  np.loadtxt(os.path.join(_HERE, "frame_positions.txt"))
    rotations =  np.loadtxt(os.path.join(_HERE, "porcupine_rotations.txt"))
    translationsb =  np.loadtxt(os.path.join(_HERE, "porcupine_positions.txt"))

    # Loop and place detectors in world
    n = 0
    for i, transrot in enumerate(zip(translations,rotations,translationsb)):
        if i < 16:
            detH = 63.5*2 + thickness*2 + window
            pmtH = 139.7 + 25.4 + 63.5*2
            pmtDeltaZ = -detH/2 - pmtH/2
            collDeltaZ = (25.4*14 + 1)/2 - detH/2
            collDeltaX = (63.5 + thickness + 25.4*5 + 1)

            t = transrot[2]
            phi, theta = transrot[1]

            phiDeg = phi


            phi = np.deg2rad(phi)
            theta = np.deg2rad(theta)

            r = [phi + np.deg2rad(180), theta]
            tlen = 47.2 #in
            tdet = transrot[2]
            pmtDelta = [pmtDeltaZ*np.sin(phi)*np.cos(theta), pmtDeltaZ*np.sin(phi)*np.sin(theta), pmtDeltaZ*np.cos(phi)]
            tP = tdet + pmtDelta

            porcupinemodules[i].placeit(parent=dummy,
                                      translation=tdet,
                                      rotation=r,
                                      rotationName='rotAx')#'HEP')
            detH = 63.5*2 + thickness*2 + window
            collDeltaZ = (25.4*14 + 1)/2 - detH/2
            collDelta = [collDeltaZ*np.sin(phi)*np.cos(theta), collDeltaZ*np.sin(phi)*np.sin(theta), collDeltaZ*np.cos(phi)]
            tC = tdet + collDelta
            porcupineColls[i].placeit(parent=dummy,
                                      translation=tC,
                                      rotation=r,
                                      rotationName='rotAx')#'HEP')
            porcupinePMTs[i].placeit(parent=dummy,
                                      translation=tP,
                                      rotation=r,
                                      rotationName='rotAx')

            r1 = [phi + np.deg2rad(180), theta]
            rb = [np.deg2rad(90), theta]
            print('i = ',i)
            bbt = transrot[2]
            if phiDeg == 18:
                bbt = transrot[2]
                braceDeltaZ = (-(25.4*26 + 1)/2 + 200 - detH/2)/2#(-(25.4*26 + 1)/2 - 40 + 2*(25.4*14 ) - detH/2)/2
                braceDelta = [braceDeltaZ*np.sin(phi)*np.cos(theta), braceDeltaZ*np.sin(phi)*np.sin(theta), braceDeltaZ*np.cos(phi)]
                bbt[2] += braceDelta[2]
                bbt[0] += braceDelta[0]
                bbt[1] += braceDelta[1]
                bbt[0:2] += (collDeltaX + 40)*np.array([(np.cos(theta)),(np.sin(theta))])
                braces[i].placeit(parent=dummy,
                              translation=bbt,
                              rotation=r1,
                              rotationName='rotAx')
                '''
                tspine = bbt
                tspine[0] = collDeltaX + 40
                tspine[2] += pmtDeltaZ
                tspine[2] += -((25.4*26 + 1) + (25.4*tlen + 1)/2 - (25.4*14 + 1) - detH) + 40

                pmtDeltaZ = -detH/2 - pmtH
                tBn = tdet[2] + pmtDeltaZ*np.cos(phi)/2
                braceDelta = [((25 + 1)/2)*np.cos(theta), ((25 + 1)/2)*np.sin(theta), -tBn]
                #ki = transrot[2]
                ki = braceDelta
                tenin[tn].placeit(parent=dummy,
                              translation=ki,
                              rotation=rb,
                              rotationName='rotAx')
                tn += 1
                '''
            elif phiDeg == 36:
                bbt = transrot[2]
                braceDeltaZ = (-(25.4*26 + 1)/2 - 80 - detH/2)/2
                braceDelta = [braceDeltaZ*np.sin(phi)*np.cos(theta), braceDeltaZ*np.sin(phi)*np.sin(theta), braceDeltaZ*np.cos(phi)]
                bbt[2] += braceDelta[2]
                bbt[0] += braceDelta[0]
                bbt[1] += braceDelta[1]
                bbt[0:2] += (collDeltaX + 80)*np.array([(np.cos(theta)),(np.sin(theta))])
                braces[i].placeit(parent=dummy,
                              translation=bbt,
                              rotation=r1,
                              rotationName='rotAx')
            elif phiDeg == 54:
                bbt = transrot[2]
                braceDeltaZ = -((25.4*14 ) )
                braceDelta = [braceDeltaZ*np.sin(phi)*np.cos(theta), braceDeltaZ*np.sin(phi)*np.sin(theta), braceDeltaZ*np.cos(phi)]
                bbt[2] += braceDelta[2]
                bbt[0:2] += (collDeltaX - 80)*np.array([(np.cos(theta)),(np.sin(theta))])
                braces[i].placeit(parent=dummy,
                              translation=bbt,
                              rotation=r1,
                              rotationName='rotAx')
            elif phiDeg == 0:
                print('phi deg = ', phiDeg)
                bbt = transrot[2]
                phi, theta = transrot[1]
                phi = np.deg2rad(phi)
                theta = np.deg2rad(theta)
                bbt = transrot[2]
                braceDeltaZ = (-(25.4*26 + 1)/2 + (25.4*14 +1) - detH/2)
                braceDelta = [braceDeltaZ*np.sin(phi)*np.cos(theta), braceDeltaZ*np.sin(phi)*np.sin(theta), braceDeltaZ*np.cos(phi)]
                bbt[2] += (braceDelta[2])
                bbt[0:2] += (collDeltaX + 40)*np.array([(np.cos(theta)),(np.sin(theta))])
                braces[i].placeit(parent=dummy,
                              translation=bbt,
                              rotation=r1,
                              rotationName='rotAx')

                tspine = bbt
                tspine[0] = collDeltaX + 40
                tspine[2] += pmtDeltaZ
                tspine[2] += -((25.4*26 + 1) + (25.4*tlen + 1)/2 - (25.4*14 + 1) - detH) + 40
                spine.placeit(parent=dummy,
                              translation=tspine,
                              rotation=None)

                transFloor = tspine.copy()
                transFloor[2] +=  -((((25.4*26 + 1) + (25.4*tlen + 1)/2 - (25.4*14 + 1) - detH) + 40)/2 + 79 + 80 + 25.4*12)

                # Place in world
                floor.placeit(parent=dummy,
                             translation=[0, 0, transFloor[2]],
                             rotation=[np.deg2rad(0),np.deg2rad(0)],
                             rotationName='rotAx')


                rot = [[90,0],[90,90],[90,180],[90,270]]
                rot1 = [[90,90],[90,180],[90,270],[90,0]]
                t11 = tspine
                deltaB = 40+1
                t11[2] += -(((25.4*26 + 1) + (25.4*tlen + 1)/2 - (25.4*14 + 1) - detH) + 40)/2 + 25 + 80#((25.4*tlen + 1)/2 - 80 - (25.4*14 ) - detH/2) + 80 + 40
                t2 = tspine
                t2[2] +=  -(((25.4*26 + 1) + (25.4*tlen + 1)/2 - (25.4*14 + 1) - detH) + 40)/2 + 25 + 80#((25.4*tlen + 1)/2 - 80 - (25.4*14 ) - detH/2) + 80 + 40
                for l in range(4):
                    if l == 0:
                        tshift = np.array([(545+40)*np.cos(r[1])+collDeltaX, (500-40)*np.sin(r[1])])
                    elif l == 2:
                        tshift = np.array([(545+40)*np.cos(r[1])+collDeltaX + 50, (500-40)*np.sin(r[1])])
                    elif l == 1:
                        tshift = np.array([(545+40)*np.cos(r[1])+collDeltaX + 25, (525+40)*np.sin(r[1])])
                    else:
                        tshift = np.array([(545+40)*np.cos(r[1])+collDeltaX + 25, (525+40)*np.sin(r[1])])
                    r = [np.deg2rad(k) for k in rot1[l]]
                    t11[0:2] = tshift
                    legSide[l].placeit(parent=dummy,
                                  translation=t11,
                                  rotation=r,
                                  rotationName='rotAx')



                    rotation = rot1[l]
                    r = [np.deg2rad(k) for k in rotation]
                    tshift = np.array([(288.)*np.cos(r[1])+collDeltaX+25, (288.)*np.sin(r[1])])
                    t2[0:2] = tshift
                    legCross[l].placeit(parent=dummy,
                                  translation=t2,
                                  rotation=r,
                                  rotationName='rotAx')

            elif phiDeg == 72:
                bbt = transrot[2]
                print('phi = ', phiDeg)
                braceDeltaZ = -(1.5*(25.4*14 + detH ) )#- detH/2)
                braceDelta = [braceDeltaZ*np.sin(phi)*np.cos(theta), braceDeltaZ*np.sin(phi)*np.sin(theta), braceDeltaZ*np.cos(phi)]
                bbt[2] += braceDelta[2]
                bbt[0:2] += (collDeltaX - 160)*np.array([(np.cos(theta)),(np.sin(theta))])
                braces[i].placeit(parent=dummy,
                            translation=bbt,
                            rotation=r1,
                            rotationName='rotAx')

    # Return
    # print 'out of loop'

    return dummy
