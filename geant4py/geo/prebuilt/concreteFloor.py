import geant4py as g4py
import numpy as np

def concreteFloor(name, copies=1):
    '''
    concrete floor used for
    scattering neutrons
    '''
    assert copies >= 1

    dummy_dim = {"x": [30 + 0.01 , 'm'], "y": [30 + 0.01, 'm'], "z": [25.4*12 + 2, 'mm']}
    dummy_primitive = g4py.geo.primitive.BoxSolid('box_dummy', dummy_dim)
    dummy_material = g4py.geo.material.Vacuum()
    dummy_color = None

    floor_dim = {"x": [30 , 'm'], "y": [30, 'm'], "z": [25.4*12 , 'mm']}
    floor_primitive = g4py.geo.primitive.BoxSolid('concrete', floor_dim)
    floor_material = g4py.geo.material.Concrete()
    floor_color = [1, 0, 0, 0.5]


    vol = np.empty(copies, dtype=object)
    for i in range(copies):
        dummy = g4py.geo.volume.Volume(name=name+"_"+str(i),
                                       primitive=dummy_primitive,
                                       material=dummy_material,
                                       sensitive=False,
                                       color=dummy_color,
                                       parent=None,
                                       translation=None,
                                       rotation=None)

        floor = g4py.geo.volume.Volume(name="floor"+"_"+str(i),
                                        primitive=floor_primitive,
                                        material=floor_material,
                                        sensitive=False,
                                        color=floor_color,
                                        parent=dummy,
                                        translation=None,
                                        rotation=None)
        vol[i] = dummy
    if copies > 1: return vol
    else: return vol[0]
