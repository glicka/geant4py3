import geant4py as g4py
import numpy as np
import math as m
import os

_HERE = os.path.dirname(os.path.abspath(__file__))

def porcupine():



    # Setup dummy
    dummy_dim = {'inner_rad':[0,'mm'],
                                   'outer_rad':[10,'m'],
                                   'sPhi':[0,'deg'],
                                   'dPhi':[360,'deg'],
                                   'sTheta':[0,'deg'],
                                   'dTheta':[180,'deg']}
    dummy_color = [2, 2, 2, 2]
    dummy_primitive = g4py.geo.primitive.Sphere('dummySphere', dummy_dim)
    dummy_material = g4py.geo.material.Vacuum()

    dummy = g4py.geo.volume.Volume(name='dummy',
                                   primitive=dummy_primitive,
                                   material=dummy_material,
                                   color=None,
                                   sensitive=False,
                                   parent=None,
                                   translation=None,
                                   rotation=None)



    # Setup single crystal
    porcupinemodules = g4py.geo.prebuilt.ej309_5in("5inDet", copies=16)
    porcupinePMTs = g4py.geo.prebuilt.hamamatsuPMT("PMT", copies=16)
    porcupineColls = g4py.geo.prebuilt.porcupineCollimatorCylinder("Collimator", copies=16)

    '''
    # Get prebuilt Concrete Slab
    floor = g4py.geo.prebuilt.concreteFloor('floor', copies=1)


    # Place in world
    floor.placeit(parent=dummy,
                 translation=None,
                 rotation=[np.deg2rad(0),np.deg2rad(0)],
                 rotationName='rotAx')
    '''


    thickness = 5. #mm
    window = 0.65 #mm
    #minermodules = g4py.geo.prebuilt.ej309_2in("detector", copies=1)


    # Read in translations and rotations from file
    translations =  np.loadtxt(os.path.join(_HERE, "porcupine_positions.txt"))
    rotations =  np.loadtxt(os.path.join(_HERE, "porcupine_rotations.txt"))

    # Loop and place detectors in world


    n = 0
    for i, transrot in enumerate(zip(translations,rotations)):
        t = transrot[0]
        phi, theta = transrot[1]

        phi = np.deg2rad(phi)
        theta = np.deg2rad(theta)

        r = [phi + np.deg2rad(180), theta]

        porcupinemodules[i].placeit(parent=dummy,
                                  translation=t,
                              rotation=r,
                              rotationName='rotAx')#'HEP')
        detH = 63.5*2 + thickness*2 + window
        collDeltaZ = (25.4*14 + 1)/2 - detH/2
        collDelta = [collDeltaZ*np.sin(phi)*np.cos(theta), collDeltaZ*np.sin(phi)*np.sin(theta), collDeltaZ*np.cos(phi)]

        tC = t + collDelta
        porcupineColls[i].placeit(parent=dummy,
                                  translation=tC,
                              rotation=r,
                              rotationName='rotAx')#'HEP')



        detH = 63.5*2 + thickness*2 + window
        pmtH = 139.7 + 25.4 + 63.5*2
        pmtDeltaZ = -detH/2 - pmtH/2
        pmtDelta = [pmtDeltaZ*np.sin(phi)*np.cos(theta), pmtDeltaZ*np.sin(phi)*np.sin(theta), pmtDeltaZ*np.cos(phi)]
        t += pmtDelta
        r1 = [phi + np.deg2rad(180), theta]

        porcupinePMTs[i].placeit(parent=dummy,
                                  translation=t,
                                  rotation=r1,
                                  rotationName='rotAx')


    # Return
    return dummy
