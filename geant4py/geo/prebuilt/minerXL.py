import geant4py as g4py
import numpy as np
import os

_HERE = os.path.dirname(os.path.abspath(__file__))



def minerXL():



    # Setup dummy
    dummy_dim = {'inner_rad':[0,'mm'],
                 'outer_rad':[1200,'mm'],
                 'height':[1200,'mm'],
                 'sPhi':[0,'deg'],
                 'dPhi':[360,'deg']}
    dummy_primitive = g4py.geo.primitive.Cylinder('minerXL', dummy_dim)
    dummy_material = g4py.geo.material.Vacuum()
    dummy = g4py.geo.volume.Volume(name='minerXL',
                                   primitive=dummy_primitive,
                                   material=dummy_material,
                                   color=None,
                                   sensitive=False,
                                   parent=None,
                                   translation=None,
                                   rotation=None)

    # Setup single crystal
    '''
    Cylinder(name='cylinder', dim={'inner_rad':[0,'mm'],
                                       'outer_rad':[1,'mm'],
                                       'height':[1,'mm'],
                                       'sPhi':[0,'deg'],
                                       'dPhi':[360,'deg']})
    '''

    minerXLmodules = g4py.geo.prebuilt.ej309_5in("detector", copies=16)
    minerXLPMTs = g4py.geo.prebuilt.hamamatsuPMT("PMT", copies=16)




    # Read in translations and rotations from file
    translations =  np.loadtxt(os.path.join(_HERE, "minerXL_positions.txt"))
    thickness = 5. #mm
    window = 0.65 #mm

    # Loop and place detectors in world
    for i, trans in enumerate(translations):
        t = trans*10

        minerXLmodules[i].placeit(parent=dummy,
                              translation=t,
                              rotation=None,
                              rotationName=None)
        
        detH = 63.5*2 + thickness*2 + window
        pmtH = 139.7 + 25.4 + 63.5*2
        pmtDeltaZ = -detH/2 - pmtH/2
        pmtDelta = [0, 0, pmtDeltaZ]
        if t[2] < 0:
            t += pmtDelta
            r1 = [np.deg2rad(180), 0]
        else:
            t -= pmtDelta
            r1 = [np.deg2rad(0), np.deg2rad(180)]

        minerXLPMTs[i].placeit(parent=dummy,
                              translation=t,
                              rotation=r1,
                              rotationName='rotAx')


    # Return
    return dummy
