import geant4py as g4py
import numpy as np

def porcupineCollimator(name, copies=1, indices=[""]):
    '''
    2x4x16in Polyethylene collimator
    Used in Porcupine
    '''
    assert copies >= 1

    thickness = 5. #mm

    dummy_dim = {"x_start": [25.4*5 + thickness + 5, 'mm'], "x_end": [25.4*5 + thickness + 25.6*4 + 5, 'mm'],
                  "y_start": [25.4*5 + thickness + 5, 'mm'], "y_end": [25.4*5 + thickness + 25.6*4 + 5, 'mm'],
                  "z_start": [25.4*16 + 1, 'mm'], "z_end": [25.4*16 + 1, 'mm']}
    dummy_primitive = g4py.geo.primitive.BoxHollow('dummy', dummy_dim)
    dummy_material = g4py.geo.material.Vacuum()
    dummy_color = None

    collimator_dim = {"x_start": [25.4*5 + thickness + 5, 'mm'], "x_end": [25.4*5 + thickness + 25.4*4 + 5, 'mm'],
                  "y_start": [25.4*5 + thickness + 5, 'mm'], "y_end": [25.4*5 + thickness + 25.4*4 + 5, 'mm'],
                  "z_start": [25.4*16, 'mm'], "z_end": [25.4*16, 'mm']}
    collimator_primitive = g4py.geo.primitive.BoxHollow('collimator', collimator_dim)
    collimator_material = g4py.geo.material.SWX210()#Polyethylene()
    #print(collimator_material)
    collimator_color = [1, 0, 0, 0.5]

    # Creating and placing volumes
    vol = np.empty(copies, dtype=object)
    collimator = np.empty(copies, dtype=object)

    for i in range(copies):


        #trans = -(63.5*2 + thickness*2 + window*2 + 1 + 139.7*2 + 88.9*2 + 63.5)/2
        ## print 'trans = ', trans
        dummy = g4py.geo.volume.Volume(name=name+"_"+str(i),
                                       primitive=dummy_primitive,
                                       material=dummy_material,
                                       sensitive=False,
                                       color=dummy_color,
                                       parent=None,
                                       translation=[0, 0, 0],
                                       rotation=None)


        collimator = g4py.geo.volume.Volume(name="collimator"+"_"+str(i),
                                        primitive=collimator_primitive,
                                        material=collimator_material,
                                        sensitive=False,
                                        color=collimator_color,
                                        parent=dummy,
                                        translation=None,
                                        rotation=None)





        vol[i] = dummy
    if copies > 1: return vol
    else: return vol[0]
