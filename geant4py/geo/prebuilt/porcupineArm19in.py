import geant4py as g4py
import numpy as np

def porcupineArm19in(name, copies=1):
    '''
    50mm x 50mm x 53.4in aluminum bar
    '''
    assert copies >= 1

    dummy_dim = {"x": [40 + 1, 'mm'], "y": [40 + 1, 'mm'], "z": [25.4*19 + 1, 'mm']}
    dummy_primitive = g4py.geo.primitive.BoxSolid('dummy', dummy_dim)
    dummy_material = g4py.geo.material.Vacuum()
    dummy_color = None

    casing_dim = {"x_start": [35.68 , 'mm'], "x_end": [40, 'mm'],
                  "y_start": [35.68, 'mm'], "y_end": [40, 'mm'],
                  "z_start": [0, 'mm'], "z_end": [25.4*19, 'mm']}
    casing_primitive = g4py.geo.primitive.BoxHollow('casing', casing_dim)
    casing_material = g4py.geo.material.element("Al")
    casing_color = [1, 0, 0, 0.5]


    vol = np.empty(copies, dtype=object)
    for i in range(copies):
        dummy = g4py.geo.volume.Volume(name=name+"_"+str(i),
                                       primitive=dummy_primitive,
                                       material=dummy_material,
                                       sensitive=False,
                                       color=dummy_color,
                                       parent=None,
                                       translation=None,
                                       rotation=None)

        casing = g4py.geo.volume.Volume(name="arm19"+"_"+str(i),
                                        primitive=casing_primitive,
                                        material=casing_material,
                                        sensitive=False,
                                        color=casing_color,
                                        parent=dummy,
                                        translation=None,
                                        rotation=None)



        vol[i] = dummy

    if copies > 1: return vol
    else: return vol[0]
