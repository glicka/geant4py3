import geant4py as g4py
import numpy as np

def ej309_5in(name, copies=1, indices=[""]):
    '''
    5 inch EJ309 modules
    Used in MINER
    name will be appended with copy number
    '''

    assert copies >= 1
    thickness = 5. #mm
    window = 0.65 #mm

    dummy_dim = {"inner_rad": [0, 'mm'], "outer_rad": [63.5 + thickness, 'mm'], "height": [63.5*2 + thickness*2 + window, 'mm'],'sPhi':[0,'deg'],'dPhi':[360,'deg']}
    dummy_primitive = g4py.geo.primitive.Cylinder('dummy', dummy_dim)
    dummy_material = g4py.geo.material.Vacuum()
    dummy_color = None#np.array([0.2, 0.2, 0.2, 0.2])#*10

    # Setting up aluminum casing

    case_dim = {"inner_rad": [63.5, 'mm'], "outer_rad": [63.5 + thickness, 'mm'], "height": [63.5*2 + thickness + window, 'mm'],'sPhi':[0,'deg'],'dPhi':[360,'deg']}
    case_primitive = g4py.geo.primitive.Cylinder('case', case_dim)
    case_color = None#np.array([0.2, 0.2, 0.2, 0.2])*10
    case_material = g4py.geo.material.element("Al")

    # Setting up detector: sensitive region
    det_dim = {"inner_rad": [0, 'mm'], "outer_rad": [63.5, 'mm'], "height": [63.5*2, 'mm'],'sPhi':[0,'deg'],'dPhi':[360,'deg']}
    det_primitive = g4py.geo.primitive.Cylinder('detector', det_dim)
    det_material = g4py.geo.material.EJ309()
    det_color = [2, 2, 2, 2]


    # Creating and placing volumes
    vol = np.empty(copies, dtype=object)
    casing = np.empty(copies, dtype=object)
    detector = np.empty(copies, dtype=object)
    vol = np.empty(copies, dtype=object)
    for i in range(copies):


        #trans = -(63.5*2 + thickness*2 + window*2 + 1 + 139.7*2 + 88.9*2 + 63.5)/2
        ## print 'trans = ', trans
        dummy = g4py.geo.volume.Volume(name=name+"_"+str(i),
                                       primitive=dummy_primitive,
                                       material=dummy_material,
                                       sensitive=False,
                                       color=dummy_color,
                                       parent=None,
                                       translation=[0, 0, 0],
                                       rotation=None)


        transCase = (dummy_dim['height'][0]/2 - case_dim['height'][0]) + case_dim['height'][0]/2#(63.5 + thickness/2 + window/2) #puts its bottom at zero
        casing = g4py.geo.volume.Volume(name="case"+"_"+str(i),
                                        primitive=case_primitive,
                                        material=case_material,
                                        sensitive=False,
                                        color=case_color,
                                        parent=dummy,
                                        translation=[0, 0 , 0],
                                        rotation=None)



        detector = g4py.geo.volume.Volume(name='det'+"_"+str(i),
                                         primitive=det_primitive,
                                         material=det_material,
                                         sensitive=True,
                                         color=det_color,
                                         parent=dummy,
                                         translation=[0, 0 , 0],
                                         rotation=None)




        vol[i] = dummy
    if copies > 1: return vol
    else: return vol[0]
