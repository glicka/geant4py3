import geant4py as g4py
import numpy as np

def hamamatsuPMT(name, copies=1, indices=[""]):
    '''
    2 inch EJ309 modules
    Used in MINER
    name will be appended with copy number
    '''

    assert copies >= 1
    thickness = 5. #mm
    window = 0.65 #mm

    dummy_dim = {"inner_rad": [0, 'mm'], "outer_rad": [76.2, 'mm'], "height": [139.7 + 25.4 + 63.5*2, 'mm'],'sPhi':[0,'deg'],'dPhi':[360,'deg']}
    dummy_primitive = g4py.geo.primitive.Cylinder('dummy', dummy_dim)
    dummy_material = g4py.geo.material.Vacuum()
    dummy_color = np.array([0.2, 0.2, 0.2, 0.2])#*10


    # Setting up PMT
    # assume all plastic for now

    pmtTop_dim = {"inner_rad": [76.2-thickness, 'mm'], "outer_rad": [76.2, 'mm'], "height": [139.7, 'mm'],'sPhi':[0,'deg'],'dPhi':[360,'deg']}
    pmtTop_primitive = g4py.geo.primitive.Cylinder('PMT_top', pmtTop_dim)
    pmtTop_material = g4py.geo.material.Polyethylene()
    pmtTop_color = [1, 1, 1, 1]



    pmtCone_dim = {"inner_rad1": [76.2-thickness, 'mm'], "outer_rad1": [76.2, 'mm'], "inner_rad2": [88.9/2 - thickness, 'mm'], "outer_rad2": [88.9/2, 'mm'], "height": [25.4, 'mm'],'sPhi':[0,'deg'],'dPhi':[360,'deg']}
    pmtCone_primitive = g4py.geo.primitive.Cone('PMT_cone', pmtCone_dim)
    pmtCone_material = g4py.geo.material.Polyethylene()
    pmtCone_color = [1, 1, 1, 1]


    pmtBottom_dim = {"inner_rad": [0, 'mm'], "outer_rad": [88.9/2, 'mm'], "height": [63.5*2, 'mm'],'sPhi':[0,'deg'],'dPhi':[360,'deg']}
    pmtBottom_primitive = g4py.geo.primitive.Cylinder('PMT_bottom', pmtBottom_dim)
    pmtBottom_material = g4py.geo.material.element("Al")
    pmtBottom_color = [1, 1, 1, 1]


    # Creating and placing volumes
    vol = np.empty(copies, dtype=object)
    inner_air = np.empty(copies, dtype=object)
    detector = np.empty(copies, dtype=object)
    pmtTop = np.empty(copies, dtype=object)
    pmtCone = np.empty(copies, dtype=object)
    pmtBottom = np.empty(copies, dtype=object)
    vol = np.empty(copies, dtype=object)
    for i in range(copies):
        #trans = -(63.5*2 + thickness*2 + window*2 + 1 + 139.7*2 + 88.9*2 + 63.5)/2
        ## print 'trans = ', trans
        dummy = g4py.geo.volume.Volume(name=name+"_"+str(i),
                                       primitive=dummy_primitive,
                                       material=dummy_material,
                                       sensitive=False,
                                       color=dummy_color,
                                       parent=None,
                                       translation=[0, 0, 0],
                                       rotation=None)


        transTop = dummy_dim['height'][0]/2 - pmtBottom_dim['height'][0]/2#(dummy_dim['height'][0]/2 - case_dim['height'][0]) + case_dim['height'][0]/2

        transBottom = -(dummy_dim['height'][0]/2 - pmtBottom_dim['height'][0] - pmtCone_dim['height'][0]) + pmtTop_dim['height'][0]/2

        pmtTop = g4py.geo.volume.Volume(name='pmtTop'+"_"+str(i),
                                         primitive=pmtTop_primitive,
                                         material=pmtTop_material,
                                         sensitive=False,
                                         color=pmtTop_color,
                                         parent=dummy,
                                         translation=[0, 0, -transBottom],#-(63.5*2 + thickness + window)],
                                         rotation=None)


        transCone = -(dummy_dim['height'][0]/2 - pmtBottom_dim['height'][0]) + pmtCone_dim['height'][0]/2#dummy_dim['height'][0]/2 - transTop + pmtCone_dim['height'][0]/2

        pmtCone = g4py.geo.volume.Volume(name='pmtCone'+"_"+str(i),
                                         primitive=pmtCone_primitive,
                                         material=pmtCone_material,
                                         sensitive=False,
                                         color=pmtCone_color,
                                         parent=dummy,
                                         translation=[0, 0, -transCone],#-(63.5*2 + thickness + window + 139.7)],
                                         rotation=None)








        pmtBottom = g4py.geo.volume.Volume(name='pmtBottom'+"_"+str(i),
                                         primitive=pmtBottom_primitive,
                                         material=pmtBottom_material,
                                         sensitive=False,
                                         color=pmtBottom_color,
                                         parent=dummy,
                                         translation=[0, 0, transTop],#-(63.5*2 + thickness + window + 139.7 + 25.4 + 63.5)],
                                         rotation=None)






        vol[i] = dummy
    if copies > 1: return vol
    else: return vol[0]
