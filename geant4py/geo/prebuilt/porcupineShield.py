import geant4py as g4py
import numpy as np

def porcupineShield(name, copies=1, indices=[""]):
    '''
    2x4x16in Polyethylene shield
    Used in Porcupine
    '''
    assert copies >= 1

    thickness = 5. #mm

    dummy_dim = {"x_start": [0, 'mm'], "x_end": [25.4*5 + thickness + 25.4*100 + 5, 'mm'],
                  "y_start": [0, 'mm'], "y_end": [25.4*5 + thickness + 25.4*100 + 5, 'mm'],
                  "z_start": [0, 'mm'], "z_end": [25.4*5 +1, 'mm']}
    dummy_primitive = g4py.geo.primitive.BoxHollow('dummy', dummy_dim)
    dummy_material = g4py.geo.material.Vacuum()
    dummy_color = None

    shield_dim = {"x_start": [0, 'mm'], "x_end": [25.4*4.75 + thickness + 25.4*100 + 5, 'mm'],
                  "y_start": [0, 'mm'], "y_end": [25.4*4.75 + thickness + 25.4*100 + 5, 'mm'],
                  "z_start": [0, 'mm'], "z_end": [25.4*2.325, 'mm']} #2. inches thick
    shield_primitive = g4py.geo.primitive.BoxHollow('shield', shield_dim)
    shield_material = g4py.geo.material.UHMWPE()#SWX201()#Polyethylene()#
    #print(shield_material)
    shield_color = [1, 0, 0, 0.5]
    '''
    case_dim = {"x_start": [25.4*4.75 + thickness + 25.4*100 + 5, 'mm'], "x_end": [25.4*5 + thickness + 25.4*100 + 5, 'mm'],
                  "y_start": [25.4*4.75 + thickness + 25.4*100 + 5, 'mm'], "y_end": [25.4*5 + thickness + 25.4*100 + 5, 'mm'],
                  "z_start": [25.4*4.75, 'mm'], "z_end": [25.4*5, 'mm']} #1/4 inches thick
    case_primitive = g4py.geo.primitive.BoxHollow('case', shield_dim)
    case_material = g4py.geo.material.B4C()#SS304()#Polyethylene()#SWX213()##Polyethylene()
    #print(shield_material)
    case_color = [1, 0, 0, 0.5]
    '''

    # Creating and placing volumes
    vol = np.empty(copies, dtype=object)
    shield = np.empty(copies, dtype=object)
    case = np.empty(copies, dtype=object)

    for i in range(copies):


        #trans = -(63.5*2 + thickness*2 + window*2 + 1 + 139.7*2 + 88.9*2 + 63.5)/2
        ## print 'trans = ', trans
        dummy = g4py.geo.volume.Volume(name=name+"_"+str(i),
                                       primitive=dummy_primitive,
                                       material=dummy_material,
                                       sensitive=False,
                                       color=dummy_color,
                                       parent=None,
                                       translation=[0, 0, 0],
                                       rotation=None)


        shield = g4py.geo.volume.Volume(name="shield"+"_"+str(i),
                                        primitive=shield_primitive,
                                        material=shield_material,
                                        sensitive=False,
                                        color=shield_color,
                                        parent=dummy,
                                        translation=None,
                                        rotation=None)
        '''
        case = g4py.geo.volume.Volume(name="case"+"_"+str(i),
                                        primitive=case_primitive,
                                        material=case_material,
                                        sensitive=False,
                                        color=case_color,
                                        parent=dummy,
                                        translation=None,
                                        rotation=None)
        '''





        vol[i] = dummy
    if copies > 1: return vol
    else: return vol[0]
