import geant4py as g4py
import numpy as np

def porcupineCollimatorCylinder(name, copies=1, indices=[""]):
    '''
    2x4x16in Polyethylene collimator
    Used in Porcupine
    '''
    assert copies >= 1

    thickness = 5. #mm

    dummy_dim = {"inner_rad": [63.5 + thickness, 'mm'], "outer_rad": [63.5 + thickness + 25.4*2.325 + 1, 'mm'], "height": [25.4*14  + 1, 'mm'],'sPhi':[0,'deg'],'dPhi':[360,'deg']}
    dummy_primitive = g4py.geo.primitive.Cylinder('dummy', dummy_dim)
    dummy_material = g4py.geo.material.Vacuum()
    dummy_color = None

    collimator_dim = {"inner_rad": [63.5 + thickness, 'mm'], "outer_rad": [63.5 + thickness + 25.4*2.325, 'mm'], "height": [25.4*14 , 'mm'],'sPhi':[0,'deg'],'dPhi':[360,'deg']}
    collimator_primitive = g4py.geo.primitive.Cylinder('collimator', collimator_dim)
    collimator_material = g4py.geo.material.UHMWPE()#SWX213()#Polyethylene()
    #print(collimator_material)
    collimator_color = [1, 0, 0, 0.5]

    # Creating and placing volumes
    vol = np.empty(copies, dtype=object)
    collimator = np.empty(copies, dtype=object)

    for i in range(copies):


        #trans = -(63.5*2 + thickness*2 + window*2 + 1 + 139.7*2 + 88.9*2 + 63.5)/2
        ## print 'trans = ', trans
        dummy = g4py.geo.volume.Volume(name=name+"_"+str(i),
                                       primitive=dummy_primitive,
                                       material=dummy_material,
                                       sensitive=False,
                                       color=dummy_color,
                                       parent=None,
                                       translation=[0, 0, 0],
                                       rotation=None)


        collimator = g4py.geo.volume.Volume(name="collimator"+"_"+str(i),
                                        primitive=collimator_primitive,
                                        material=collimator_material,
                                        sensitive=False,
                                        color=collimator_color,
                                        parent=dummy,
                                        translation=None,
                                        rotation=None)





        vol[i] = dummy
    if copies > 1: return vol
    else: return vol[0]
