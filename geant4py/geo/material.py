import Geant4 as g4
import numpy as np
import os

_HERE = os.path.dirname(os.path.abspath(__file__))
_ELEMENTSFILE = os.path.join(_HERE, "elements.txt")

g = g4.g
mole = g4.mole
cm3 = g4.cm3


def build(name, components, density):
    # Read in element data
    el = np.genfromtxt(_ELEMENTSFILE, delimiter=', ', dtype=[('Z','i8'),('symbol','S3'),('name','S15'),('mass','f8')], encoding=None)

    # Initialize material
    mat = g4.G4Material(g4.G4String(name), density, len(components))

    # Add elements either by molecular formula or mass fraction
    # Component values should sum to 1 if mass fraction
    if (sum(components.values()) == 1.):

        # Add elements to material
        for sym, mf in components.items():
            x = el[np.char.lower(el['symbol']) == np.char.lower(sym.encode('utf-8'))][0]
            try:
                mat.AddElement(g4.G4Element(g4.G4String(x['name'].decode('utf-8')), g4.G4String(x['symbol'].decode('utf-8')), np.double(x['Z']), np.double(x['mass'])*g/mole), mf)
            except:
                mat.AddElement(g4.G4Element(g4.G4String(x['name']), g4.G4String(x['symbol']), np.double(x['Z']), np.double(x['mass'])*g/mole), mf)

    # Otherwise its a molecular formula
    else:
        # Compute total mass
        total_mass = 0.
        for sym, natoms in components.items():
            x = el[np.char.lower(el['symbol']) == np.char.lower(sym.encode('utf-8'))][0]
            total_mass += natoms * x['mass']*g/mole

        # Add elements to material
        for sym, natoms in components.items():
            x = el[np.char.lower(el['symbol']) == np.char.lower(sym.encode('utf-8'))][0]
            mf = natoms * x['mass']*g/mole / total_mass
            try:
                mat.AddElement(g4.G4Element(g4.G4String(x['name'].decode('utf-8')), g4.G4String(x['symbol'].decode('utf-8')), np.double(x['Z']), np.double(x['mass'])*g/mole), mf)
            except:
                mat.AddElement(g4.G4Element(g4.G4String(x['name']), g4.G4String(x['symbol']), np.double(x['Z']), np.double(x['mass'])*g/mole), mf)
    return mat

def buildElemental(name, components, ratios, density):
    # Initialize material
    mat = g4.G4Material(g4.G4String(name), density, len(components))

    # Add elements either by molecular formula or mass fraction
    # Component values should sum to 1 if mass fraction
    if (sum(ratios) == 1.):
        # Add elements to material
        for m in range(len(components)):
            x = components[m]
            mf = ratios[m]
            #print('x = ', x)
            mat.AddElement(g4.G4Element(g4.G4String(x['name']), g4.G4String(x['symbol']), np.double(x['Z']), np.double(x['mass'])*g/mole), mf)

    # Otherwise its a molecular formula
    else:
        # Compute total mass
        total_mass = 0.
        for m in range(len(components)):
            natoms = ratios[m]
            sym = components[m]
            total_mass += natoms * sym['mass']*g/mole

        # Add elements to material
        for m in range(len(components)):
            natoms = ratios[m]
            x = components[m]
            mf = natoms * x['mass']*g/mole / total_mass
            #print('x = ', x)
            mat.AddElement(g4.G4Element(g4.G4String(x['name']), g4.G4String(x['symbol']), np.double(x['Z']), np.double(x['mass'])*g/mole), mf)
    #print('mat = ',mat)
    return mat

def element(name):
    '''
    Return base Geant4 element material
    Currently requires correct capitalization and using only the symbol
    i.e. Ge
    '''
    return g4.gNistManager.FindOrBuildMaterial(g4.G4String("G4_" + name))

def Vacuum():
    '''Define vacuum'''
    return g4.gNistManager.FindOrBuildMaterial(g4.G4String("G4_Galactic"))

def Air():
    '''Define air'''
    return g4.gNistManager.FindOrBuildMaterial(g4.G4String("G4_AIR"))

def NaI():
    ''' Define NaI material'''
    return g4.gNistManager.FindOrBuildMaterial(g4.G4String("G4_SODIUM_IODIDE"))

def CsI():
    ''' Define CsI material'''
    return g4.gNistManager.FindOrBuildMaterial(g4.G4String("G4_CESIUM_IODIDE"))

def Concrete():
    '''Define Concrete material'''
    return g4.gNistManager.FindOrBuildMaterial(g4.G4String("G4_CONCRETE"))

def CZT():
    '''Define CZT material (Cd0.9Zn10.1Te1)'''
    # Setup
    name = "CZT"
    components = {"Cd": 9, "Zn": 1, "Te": 10}
    density = 5.78*g/cm3
    # Build and return
    return build(name, components, density)

def CLLBC():
    '''Define CLLBC material (Cs2LiLaBr4.8Cl1.2:Ce)'''
    # Setup (ignore dopant, for now)
    name = "CLLBC"
    components = {"Cs": 20, "Li": 10, "La": 10, "Br": 48, "Cl": 12}
    density = 4.1*g/cm3
    # Build and return
    return build(name, components, density)

def CLYC():
    '''Define CLYC material (Cs2LiYCl6:Ce)'''
    # Setup (ignore dopant, for now)
    name = "CLYC"
    components = {"Cs": 2, "Li": 1, "Y": 1, "Cl": 6}
    density = 3.31*g/cm3
    # Build and return
    return build(name, components, density)

def EJ309():
    '''Define EJ309 material (5.43e22 H, 4.35e22 C)'''
    # Setup
    name = "EJ309"
    massFracH = (5.43*1.008)/(5.43*1.008+4.35*12.001) #No.of H Getoms/cm3, 5.43E22
    massFracC = 1-massFracH
    components = {"H": massFracH, "C": massFracC} #*1e22
    density = 0.964*g/cm3
    # Build and return
    return build(name, components, density)

def Nylon12CF():
    '''Define the 70% nylon-12/30% carbon-fiber mixture used in the CLLBC housing materials
    using nylon-12 (C12H23NO) and carbon-fiber (C3H3N)
    '''
    name = "Nylon12CF"
    components = {"C": 93, "H": 170, "N": 10, "O": 7}
    density = 1.34*g/cm3
    return build(name, components, density)

def PCB():
    '''Define PCB as 60% SiO2 (fiberglass) and 40% C21H25Cl05 (Epoxy resin)'''
    name = "PCB"
    components = {"Si": 6, "O": 32, "C": 84, "H": 100, "Cl": 4}
    density = 1.8*g/cm3
    return build(name, components, density)

def LithiumIronPhosphate():
    '''Battery material LiFeP04'''
    name = "LithiumIronPhosphate"
    components = {"Li": 1, "Fe": 1, "P": 1, "O": 4}
    density = 1.67*g/cm3
    return build(name, components, density)

def Steel():
    name = "Steel"
    components = {"Fe": 96, "C": 4}
    density = 7.85*g/cm3
    return build(name, components, density)

def Glass():
    name = "Glass"
    components = {"Si": 1, "O": 2}
    density = 2.6*g/cm3
    return build(name, components, density)

def CameraLens():
    name = "CameraLens"
    components = {"Si": 10000, "O": 26954, "C": 3, "N": 22659, "Ar": 384}
    density = 1.82*g/cm3
    return build(name, components, density)

def Lexan():
    '''Define lexan'''
    return g4.gNistManager.FindOrBuildMaterial(g4.G4String("G4_POLYCARBONATE"))

def Nylon12Al():
    '''Nylon-aluminum material that makes up the non-cllbc lamp walls'''
    name = "Nylon12Al"
    components = {"H": 23, "O": 1, "C": 12, "N": 1, "Al": 1}
    density = 1.855*g/cm3
    return build(name, components, density)

def Polyethylene():
    '''Define polyethylene'''
    return g4.gNistManager.FindOrBuildMaterial(g4.G4String("G4_POLYETHYLENE"))

def SWX213():
    '''Define shieldwerx 213 poly: (CH2)n'''
    # Setup
    name = "SWX213"
    #weightRatio = (7.9*1.008)/(7.9*1.008+4.35*12.001) #No.of H Getoms/cm3, 5.43E22
    components = {"H": .1438, "C": 0.8562} #*1e22
    density = 0.92*g/cm3
    # Build and return
    return build(name, components, density)

def UHMWPE():
    '''Define ultra-high molecular weight pe: (CH2)n'''
    # Setup
    name = "UHMWPE"
    #weightRatio = (7.9*1.008)/(7.9*1.008+4.35*12.001) #No.of H Getoms/cm3, 5.43E22
    components = {"H": .1438, "C": 0.8562} #*1e22
    density = 0.97*g/cm3
    # Build and return
    return build(name, components, density)

def SWX201():
    '''Define shieldwerx 213 poly: (CH2)n'''
    # Setup
    name = "SWX201"
    #weightRatio = (7.9*1.008)/(7.9*1.008+4.35*12.001) #No.of H Getoms/cm3, 5.43E22

    components = {"H": .117, "B10": 0.0098, "B11": 0.0402, "C": 0.833} #*1e22
    density = 0.95*g/cm3
    # Build and return
    return build(name, components, density)

def SS304():
    '''Define 304 stainless steel: 0.07% C, 0.75% Si, 2% Mn, 0.045% P, 0.03% S, 17.5% Cr, 8% Ni, 0.1% N'''
    # Setup
    name = "SS304"
    #weightRatio = (7.9*1.008)/(7.9*1.008+4.35*12.001) #No.of H Getoms/cm3, 5.43E22

    components = {"C": .0007, "Si": 0.0075, "Mn": 0.02, "P": 0.00045, "S": 0.0003, "Cr": 0.175, "Ni": 0.08, "N": 0.001, "Fe": 0.71505} #*1e22
    density = 0.95*g/cm3
    # Build and return
    return build(name, components, density)

def B4C():
    '''Define boron carbide'''
    # Setup
    name = "BoronCarbide"
    #weightRatio = (7.9*1.008)/(7.9*1.008+4.35*12.001) #No.of H Getoms/cm3, 5.43E22

    components = {"B10": 4, "C": 1} #*1e22
    density = 2.52*g/cm3
    # Build and return
    return build(name, components, density)

def SWX210():
    ''' '''
    # Setup
    name = "SWX210"
    #weightRatio = (7.9*1.008)/(7.9*1.008+4.35*12.001) #No.of H Getoms/cm3, 5.43E22
    #g4.gNistManager.FindOrBuildMaterial(g4.G4String("G4_POLYETHYLENE"))
    #30
    components = {"H": .087, "B10": 0.057, "B11": 0.243, "C": 0.613} #*1e22
    density = 1.19*g/cm3
    # Build and return
    return build(name, components, density)
