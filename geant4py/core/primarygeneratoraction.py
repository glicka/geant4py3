import Geant4 as g4
import numpy as np
from geant4py.source.source import Source
from scipy.stats import poisson
import math


class MyPrimaryGeneratorAction(g4.G4VUserPrimaryGeneratorAction):
    "My Primary Generator Action"

    def __init__(self, source=Source()):
        g4.G4VUserPrimaryGeneratorAction.__init__(self)

        # Initialize source term
        self.set_source(source)
        # print(self.source.energy)

        # x=np.linspace(self.source.minE,self.source.maxE,1000)
        self.enrg = np.linspace(self.source.minE, self.source.maxE, 1000)
        j1 = 1.006e-6 * np.exp(
            -0.35 * (np.log(self.enrg)) ** 2 + 2.1451 * np.log(self.enrg)
        )
        j2 = 1.011e-3 * np.exp(
            -0.4106 * (np.log(self.enrg)) ** 2 - 0.6670 * np.log(self.enrg)
        )
        M = j1 + j2  # cm^-2 s^-1

        pdf = M / sum(M)
        self.cdf = np.cumsum(pdf)
        self.cdf /= max(self.cdf)

        # x = np.linspace(0.,1.,1000)
        # y = np.linspace(0.1,1000.,1000)
        # self.MuonAngularFlux = 0.14*(x**2.94)*((x*y+3.38)**(-2.7))*(1./(1.+(1.11/115.)*(x*y+3.38))+0.054/(1.+(1.11/850.)*(x*y+3.38)))

        # Get particle
        particle_table = g4.G4ParticleTable.GetParticleTable()
        particle = particle_table.FindParticle(g4.G4String(self.source.particle))

        # Setup beam
        beam = g4.G4ParticleGun()
        beam.SetParticleEnergy(
            self.source.energy * getattr(g4, self.source.energy_unit)
        )
        beam.SetParticleMomentumDirection(g4.G4ThreeVector(0, 0, -1))
        beam.SetParticleDefinition(particle)
        beam.SetParticlePosition(g4.G4ThreeVector(0, 0, 1 * g4.m))

        self.particleGun = beam

    def GeneratePrimaries(self, event):
        """

        if self.detectorConstruction is not None:
            self.myDetector = self.detectorConstruction
            #worldRadius = self.detectorConstruction.GetWorldRadius()/g4.mm   #G4cout << " worldRadius=     " << worldRadius    << G4endl;
            self.sheetType = "detectorWide"
            self.SetSheetLength(1.*g4.m)
        """

        # Far field
        if self.source.field == "far":
            pos, direc = self.far_field()
            # print('xdir = ',direc.z)

        elif self.source.field == "near":
            pos, direc = self.near_field()

        elif self.source.field == "line":
            pos, direc = self.line()

        elif self.source.field == "isotropic":
            pos, direc = self.isotropic()
        elif self.source.field == "isotropicResp":
            pos, direc = self.isotropicResp()
        elif self.source.field == "isotropicPos":
            pos, direc = self.isotropicPos()
        elif self.source.field == "isotropicNeg":
            pos, direc = self.isotropicNeg()
        elif self.source.field == "neutron":
            pos, direc = self.GenerateNeutronPrimaries()

        else:
            raise Exception("Unrecognized source type")

        _x = g4.G4ThreeVector(direc.x, direc.y, direc.z)
        _y = g4.G4ThreeVector(pos.x, pos.y, pos.z)

        # Calculate energy for particle

        rndm = g4.G4UniformRand()

        energy = np.interp(rndm, self.cdf[:], self.enrg)

        self.source.energy = energy

        # print('energy = ', self.source.energy)
        # Get particle
        particle_table = g4.G4ParticleTable.GetParticleTable()
        particle = particle_table.FindParticle(g4.G4String(self.source.particle))

        self.particleGun.SetParticleDefinition(particle)
        self.particleGun.SetParticleEnergy(
            self.source.energy * getattr(g4, self.source.energy_unit)
        )
        self.particleGun.SetParticleMomentumDirection(_x)
        self.particleGun.SetParticlePosition(_y)
        self.particleGun.GeneratePrimaryVertex(event)

    def far_field(
        self,
    ):
        # Set the direction of the rays, given theta and phi (calculate position, take all negative values)
        r = self.source.source_dist * getattr(g4, self.source.length_unit)
        theta = self.source.theta * getattr(g4, self.source.angle_unit)
        phi = self.source.phi * getattr(g4, self.source.angle_unit)
        direc = g4.G4ThreeVector(
            -r * np.cos(phi) * np.sin(theta),
            -r * np.sin(phi) * np.sin(theta),
            -r * np.cos(theta),
        )

        # Uniformly sample a disk that just covers the entire detector (start at z-axis (0,0,1) and then rotate)
        rand_r = g4.G4UniformRand()
        rand_theta = 2.0 * np.pi * g4.G4UniformRand()
        rad = self.source.source_rad * getattr(g4, self.source.length_unit)
        source_pos = g4.G4ThreeVector(
            rad * np.sqrt(rand_r) * np.cos(rand_theta),
            rad * np.sqrt(rand_r) * np.sin(rand_theta),
            r,
        )

        # Get position by rotating the z-oriented disk
        pos = source_pos.rotateY(theta).rotateZ(phi)

        return pos, direc

    def near_field(
        self,
    ):
        r = self.source.source_dist * getattr(g4, self.source.length_unit)
        theta = self.source.theta * getattr(g4, self.source.angle_unit)
        phi = self.source.phi * getattr(g4, self.source.angle_unit)

        # Source position (cone vertex)
        pos = g4.G4ThreeVector(
            r * np.cos(phi) * np.sin(theta),
            r * np.sin(phi) * np.sin(theta),
            r * np.cos(theta),
        )

        # Cone beam with vertex at (0, 0)
        cone_rad = self.source.source_rad * getattr(g4, self.source.length_unit)
        opening_angle = np.arctan(cone_rad / r)
        # open a cone of degree theta centered on positive z-axis
        z = 1 - (1 - np.cos(opening_angle)) * g4.G4UniformRand()
        cone_phi = 2.0 * np.pi * g4.G4UniformRand()
        initial_direction = g4.G4ThreeVector(
            np.sqrt(1 - z * z) * np.cos(cone_phi),
            np.sqrt(1 - z * z) * np.sin(cone_phi),
            -z,
        )

        # Rotate direction to  source location
        direction = initial_direction.rotateY(theta).rotateZ(phi)
        # print('direction = ',direction)

        return pos, direction

    def isotropic(
        self,
    ):
        r = self.source.source_dist * getattr(g4, self.source.length_unit)
        theta = self.source.theta * getattr(g4, self.source.angle_unit)
        phi = self.source.phi * getattr(g4, self.source.angle_unit)

        pos = g4.G4ThreeVector(
            r * np.cos(phi) * np.sin(theta),
            r * np.sin(phi) * np.sin(theta),
            r * np.cos(theta),
        )

        dir_phi = 2.0 * np.pi * g4.G4UniformRand()
        dir_theta = np.arccos((2.0 * g4.G4UniformRand()) - 1.0)

        direction = g4.G4ThreeVector(
            np.cos(dir_phi) * np.sin(dir_theta),
            np.sin(dir_phi) * np.sin(dir_theta),
            np.cos(dir_theta),
        )

        return pos, direction

    def isotropicPos(
        self,
    ):
        # Uniformly sample a disk that just covers the entire detector (start at z-axis (0,0,1) and then rotate)
        # Set the direction of the rays, given theta and phi (calculate position, take all negative values)
        r = self.source.source_dist * getattr(g4, self.source.length_unit)
        theta = self.source.theta * getattr(g4, self.source.angle_unit)
        phi = self.source.phi * getattr(g4, self.source.angle_unit)
        rand_r = g4.G4UniformRand()
        rand_theta = 2.0 * np.pi * g4.G4UniformRand()
        rad = self.source.source_rad * getattr(g4, self.source.length_unit)
        source_pos = g4.G4ThreeVector(
            rad * np.sqrt(rand_r) * np.cos(rand_theta),
            rad * np.sqrt(rand_r) * np.sin(rand_theta),
            r,
        )

        # Get position by rotating the z-oriented disk
        pos = source_pos.rotateY(theta).rotateZ(phi)

        dir_phi = 2.0 * np.pi * g4.G4UniformRand()
        dir_theta = np.arccos((2.0 * g4.G4UniformRand()) - 1.0)

        initial_direction = g4.G4ThreeVector(
            np.cos(dir_phi) * np.sin(dir_theta),
            np.sin(dir_phi) * np.sin(dir_theta),
            -abs(np.cos(dir_theta)),
        )

        # Rotate direction to  source location
        direction = initial_direction.rotateY(theta).rotateZ(phi)
        direction /= np.sqrt(direction.x**2 + direction.y**2 + direction.z**2)

        return pos, direction

    def GenerateNeutronPrimaries(
        self,
    ):
        theta = 0.0
        phi = 0
        cosTheta = 0
        energy = -1
        distanceToSurface = -1.0
        distanceToWorldEdge = 0.0
        samplingCounter = 0

        # 	keep grabbing energy, cosTheta until you get a muon that survives
        # 	and a valid radius to the surface
        x = 0
        y = 0
        z = 0
        posX = 0
        posY = 0
        posZ = 0
        dirX = 0
        dirY = 0
        dirZ = 0
        sampledEnergy = (
            -1.0
        )  # Use this local variable to fill the TH2F * succesfulSampling with the sampled surface energy (before substracting energy loss)

        while energy < 0 or distanceToSurface < 0:
            # 	grab a random cosTheta and energy
            # 0.14*pow(x,2.94)*pow(x*y+3.38,-2.7)*(1./(1.+(1.11/115.)*(x*y+3.38))+0.054/(1.+(1.11/850.)*(x*y+3.38)))"

            # MuonAngularFlux.GetRandom2(cosTheta,energy);
            cosTheta = (2.0 * g4.G4UniformRand()) - 1.0
            # std::cout<<"cosTheta = " <<cosTheta<<std::endl;
            # std::cout<<"energy = " <<energy<<std::endl;
            # added by BCP:
            samplingCounter += 1
            # CheckFluxSampling.Fill(cosTheta,energy);
            sampledEnergy = np.interp(g4.G4UniformRand(), self.cdf[:], self.enrg)

            # self.source.energy = energy

            # 	grab a random theta/phi
            theta = acos(cosTheta)
            phi = 2.0 * np.pi * g4.G4UniformRand()

            # For debugging
            # theta = 0.;//75.*deg;//TMath::Pi()/2.;
            # phi = 0.*deg;
            # std::cout<<"theta = " <<theta<<std::endl;
            # std::cout<<"phi = " <<phi<<std::endl;

            # ----------------------------------------------------------------------
            x = 2.0 * GetGeneratingSheetX() * (g4.G4UniformRand() - 0.5)
            y = 2.0 * GetGeneratingSheetY() * (g4.G4UniformRand() - 0.5)

            # For debugging
            # x = 0.;
            # y = 0.;
            # std::cout<<"x= "<< x<<std::endl;
            # std::cout<<"y= "<< y<<std::endl;
            # std::cout<<"GetGeneratingSheetX()= "<< GetGeneratingSheetX()<<std::endl;
            # std::cout<<"GetGeneratingSheety()= "<< GetGeneratingSheetY()<<std::endl;

            # now rotate the plane by theta/phi
            # BCP: for illustration: if phi = 0 the rotation is around y-axis, leaving posY = y.
            posX = (x * cos(theta) - z * sin(theta)) * cos(phi) + y * sin(
                phi
            )  # //std::cout<<"posX= "<<posX<<std::endl;
            posY = -(x * cos(theta) - z * sin(theta)) * sin(phi) + y * cos(
                phi
            )  # //std::cout<<"posY= "<<posY<<std::endl;
            posZ = x * sin(theta) + z * cos(
                theta
            )  # //std::cout<<"posZ= "<<posZ<<std::endl;

            # calculate the direction of the muon to the map
            dirX = sin(theta) * cos(phi)  # //std::cout<<"dirX= "<<dirX<<std::endl;
            dirY = sin(theta) * sin(phi)  # //std::cout<<"dirY= "<<dirY<<std::endl;
            dirZ = cos(theta)  # //std::cout<<"dirZ= "<<dirZ<<std::endl;

            distanceToWorldEdge = -posZ * dirZ - posX * dirX - posY * dirY
            distanceToWorldEdge += np.sqrt(
                (posZ * dirZ + posX * dirX + posY * dirY) ** 2
                - (posX**2)
                - (posY**2)
                - (posZ**2)
                + (myDetector.GetWorldRadius() ** 2)
            )

            # 	project back to the world radius
            posX += (
                distanceToWorldEdge * dirX
            )  # //std::cout<<"posX= "<<posX<<std::endl;
            posY += (
                distanceToWorldEdge * dirY
            )  # //std::cout<<"posY= "<<posY<<std::endl;
            posZ += (
                distanceToWorldEdge * dirZ
            )  # //std::cout<<"posZ= "<<posZ<<std::endl;

            # std::cout<<"myDetector.GetSNS().GetGroundLevelZ() ="<<myDetector.GetSNS().GetGroundLevelZ()<<std::endl;
            # std::cout<<"myDetector.GetGroundDepthZ() ="<<myDetector.GetGroundDepthZ()<<std::endl;

            if (
                posZ < myDetector.GetSNS().GetGroundLevelZ()
                and posZ > -myDetector.GetGroundDepthZ()
            ):
                distanceToSurface = (
                    myDetector.GetSNS().GetGroundLevelZ() - posZ
                ) / cos(theta)
                energy -= (
                    0.217
                    * (distanceToSurface - distanceToWorldEdge)
                    * (myDetector.GetSNS().GetSoilDensity() / (g4.g / g4.cm3))
                )
            elif posZ < -myDetector.GetGroundDepthZ():
                distanceToSurface = -1.0
                energy = -1.0
            else:
                distanceToSurface = 0.0
        # succesfulSampling.Fill(cosTheta,sampledEnergy)
        pos = g4.G4ThreeVector(posX, posY, posZ)
        direc = g4.G4ThreeVector(-dirX, -dirY, -dirZ)

        return pos, direc

    def SetSheetLength(self, sheetLength):
        if self.sheetType == "detectorWide":
            shieldHalfXSize = (
                self.myDetector.GetShieldHalfXSize() / g4.mm
            )  # getattr(g4, self.source.length_unit)
            shieldHalfYSize = self.myDetector.GetShieldHalfYSize() / g4.mm
            shieldHalfZSize = self.myDetector.GetShieldHalfZSize() / g4.mm
            shieldDiagonal = np.sqrt(
                (shieldHalfXSize**2) + (shieldHalfYSize**2) + (shieldHalfZSize**2)
            )
            self.GeneratingSheetX = shieldDiagonal * 1.05
            self.GeneratingSheetY = GeneratingSheetX
        elif self.sheetType == "concreteWide":
            self.GeneratingSheetX = (
                myDetector.GetSNS().GetBasementWidth() / mm
                + 2 * myDetector.GetSNS().GetConcreteWallTickness() / mm
            )
            self.GeneratingSheetY = GeneratingSheetX
        elif self.sheetType == "customWide":
            self.GeneratingSheetX = sheetLength / g4.mm
            self.GeneratingSheetY = sheetLength / g4.mm

        # return GeneratingSheetX, GeneratingSheetY

    def set_source(self, source):
        self.source = source

        # Set random seed
        if self.source.random_seed is not None:
            g4.HepRandom.setTheSeed(self.source.random_seed)

    def get_source(self):
        return self.source

    """
    def set_det(self, detectorConstruction):
        self.detectorConstruction = detectorConstruction
    """
