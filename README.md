# geant4py

Geant4 framework written in python using BOOST bindings. Current support is for python2.7 (with a provided anaconda environment) on macOS and Ubuntu 16. Tested with Geant4.10.2 and up.

## Installation

#### Anaconda environment

- If you don't already have Anaconda installed
    ```
    $ wget https://repo.anaconda.com/archive/Anaconda2-2019.03-Linux-x86_64.sh
    $ bash Anaconda2-2019.03-Linux-x86_64.sh
    $ rm -rf Anaconda2-2019.03-Linux-x86_64.sh
    ```

- Setup the geant4py27 conda environment
    ```
    $ conda env create -f installs/environment27.yml
    $ conda activate geant4py27
    ```

#### Geant4

- If Geant4 is not installed, run provided install script
    ```
    $ sh installs/install_geant4.sh <path/to/G4install>
    ```

- Source Geant4 (add these to your .bashrc)
    ```
    $ . <path/to/G4install/Geant4.10.5.p0/geant4-install/share/Geant4-10.5.0/geant4make/geant4make.sh>
    $ . <path/to/G4install/Geant4.10.5.p0/geant4-install/bin/geant4.sh>
    ```

#### Geant4 python environment

- Run provided install script
    ```
    $ sh installs/install_g4py.sh <path/to/G4install/Geant4.10.5.p0/geant4-source>
    ```

- Test install
    ```
    $ python -c "import Geant4"
    ```

#### geant4py

- Run the setup script
    ```
    $ python setup.py <install/develop>
    ```

- Test install
    ```
    $ python scripts/prism.py -n 100 -out test
    ```

## Multiprocessing

- Simulations can be locally multiprocessed across certain parameters.
- For example, for a simulation of a single energy and single direction, the simulation can be multiprocessed across the number of particles
    ```
    $ python scripts/prism.py -n 1000000 -E 662 -healpix 16 0 -out test -mp
    ```
- For a simulation of multiple energies and single direction, the simulation can be multiprocessed across the energies
    ```
    $ python scripts/prism.py -n 1000000 -E 60 186 356 662 1001 1173 1332 -healpix 16 0 -out test -mp
    ```
- For a simulation of a single energy and multiple directions, the simulation can be multiprocessed across the directions (the -1 in the healpix call will simulate all directions given the nside parameter)
    ```
    $ python scripts/prism.py -n 1000000 -E 662 -healpix 16 -1 -out test -mp
    ```        
- The multiprocessed output data will be written to separates files and then merged together into one file. Therefore this is some additional processing that needs to happen when reading in the data. An example notebook is provided to show this.

## Code Structure

..

## Geometry, Sources, Physics

..

## System Response Simulations

...

## Output Data

..

## Examples

..
