#!/bin/bash

#### TO RUN:
#### bash install_g4py.sh /path/to/geant4-source/

# Check if python 3
if [ ${py_major} == 3 ]
then
    echo "Currently no support for python3...Try using the provided python2 conda environment."
    exit
fi
#a = dpkg -s libboost-dev
pkg="libboost-all-dev"

if [ dpkg -s $pkg ]
then
   echo $pkg installed
else
   sudo apt-get install libboost-all-dev
fi

# Get geant4-source directory from command line
geant4src=$1

# Go to Geant4 envrionment directory
cd "${geant4src}/environments"

# Copy g4py directory to new directory, name based on python version
py_major=$(python -c 'import platform; print(platform.python_version_tuple()[0])')
py_minor=$(python -c 'import platform; print(platform.python_version_tuple()[1])')
cp -r g4py/ "g4py${py_major}${py_minor}"

# Go into directory, update for conda libs, cmake, bugs, phyiscs lists, etc.
cd "g4py${py_major}${py_minor}"

### Change the LD library path for boost BEFORE running install_g4py.sh to include additional path 
# Add conda lib enforcement lines to top level CMakeList
sed -i '' -e '/set(CMAKE_CXX_STANDARD 11)/ a\
\
# Conda paths\
set(BOOST_ROOT "$/home/glicka/boost_1_71_0")
set(BOOST_INCLUDE_DIRS "${BOOST_ROOT}/include")
set(BOOST_LIBRARY_DIRS "${BOOST_ROOT}/stage/lib/")
set(ENV{BOOST_INCLUDEDIR} "${BOOST_ROOT}/include")
set(ENV{BOOST_LIBRARYDIR} "${BOOST_ROOT}/lib")
set(XERCESC_ROOT_DIR "$ENV{CONDA_PREFIX}")\
' CMakeLists.txt

# For lib64 to lib in cmake install path
sed -i '' -e 's/lib64/lib/g' cmake/Modules/SetInstallPath.cmake

# Add G4PhysicsListFactory
echo \
'''
#include <boost/python.hpp>
#include "G4PhysListFactory.hh"

using namespace boost::python;

// ====================================================================
// module definition
// ====================================================================
void export_G4PhysListFactory()
{
  class_<G4PhysListFactory, G4PhysListFactory*>
    ("G4PhysListFactory", "phys list factory")
    // ---
    .def("GetReferencePhysList", &G4PhysListFactory::GetReferencePhysList,
         return_internal_reference<>())
    ;
}

// ====================================================================
// module definition
// ====================================================================
void export_PhysicsLists();
void export_G4PhysListFactory();

BOOST_PYTHON_MODULE(G4physicslists)
{
  export_PhysicsLists();
  export_G4PhysListFactory();
}
''' \
> source/physics_lists/pymodG4physicslists.cc

# # Check if python 3
# if [ ${py_major} == 3 ]
# then
#     # Remove pyc/o files from cmake installs
#     for f in $(find . -name 'CMakeLists.txt')
#     do
# 	sed -i '' -e '/^install (FILES ${PYC_FILES}/ d' $f
# 	sed -i '' -e '/^install (FILES ${PYO_FILES}/ d' $f
#     done
#
#     # Comment out G4LossTable b/c it complains about it
#     sed -i '' -e 's/.*G4LossTableManager/#&/' source/python3/__init__.py
# fi

# Make and go into build directory
mkdir build
cd build

# Number of cores
ncores=$(python -c "import multiprocessing as mp; print(mp.cpu_count())")

# Build
cmake ../
make "-j${ncores}"
make install

# Soft link Geant4 install into conda environment
rm -rf "${CONDA_PREFIX}/lib/python${py_major}.${py_minor}/site-packages/Geant4"
ln -sf "${geant4src}/environments/g4py${py_major}${py_minor}/lib/Geant4" "${CONDA_PREFIX}/lib/python${py_major}.${py_minor}/site-packages/Geant4"
