import geant4py as g4py
import numpy as np
import matplotlib.pyplot as plt
import Geant4 as g4
from Geant4 import gRunManager, HepRandom, G4String, G4physicslists, G4UImanager
from Geant4 import *

phiRot = np.arange(0,95,5)
# Get prebuilt world volume

'''
world = g4py.geo.prebuilt.world(primitive='sphere',
                                material=g4py.geo.material.Vacuum())
'''
world_primitive = g4py.geo.primitive.Sphere('world', dim={'inner_rad':[0,'m'],
                                                 'outer_rad':[50, 'm'],
                                                 'sPhi':[0,'deg'],
                                                 'dPhi':[360,'deg'],
                                                 'sTheta':[0,'deg'],
                                                 'dTheta':[180,'deg']})
world = g4py.geo.volume.Volume(name='world',
                              primitive=world_primitive,
                              material=g4py.geo.material.Vacuum(),
                              sensitive=False,
                              color=None,
                              parent=None,
                              translation=None,
                              rotation=None)
# Get prebuilt MINER

porcupine = g4py.geo.prebuilt.porcupineSingle()
# print porcupine

# Place in world

porcupine.placeit(parent=world,
             translation=None,
             rotation=[np.deg2rad(75),0],
             rotationName='rotAx')

# Setup source
'''
python scripts/miner.py -n 5000000 -E 2000 -healpix 16 0 -mp
'''
elist = [0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10]
source = g4py.Source(field='far',
                     particle='neutron',
                     energy=[2.5, 3, 3.5, 4],#4.6,
                     energy_unit='MeV',
                     direction=[0,0], #should be nside if direction_type is healpix
                     direction_type='angle',
                     num_particles=int(5e5),
                     source_rad=120.65,#71.685,
                     source_dist=500,
                     length_unit='cm',
                     random_seed=42,
                     outputfile='output/porcupine1Col14inCone5e5ParticlesFatBeamRot75')


# Send to sim
run = g4py.Run(world=world,
               source=source,
               check_overlaps=True,
               track_secondaries=True,
               vis_driver=None,#'oglsx',
               physics_list='QGSP_BERT_HP',
               process_tracking = ['ioni', 'elastic'])
