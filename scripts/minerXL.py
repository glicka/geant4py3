import geant4py as g4py
import numpy as np

world_primitive = g4py.geo.primitive.Sphere('world', dim={'inner_rad':[0,'m'],
                                                 'outer_rad':[50, 'm'],
                                                 'sPhi':[0,'deg'],
                                                 'dPhi':[360,'deg'],
                                                 'sTheta':[0,'deg'],
                                                 'dTheta':[180,'deg']})
world = g4py.geo.volume.Volume(name='world',
                              primitive=world_primitive,
                              material=g4py.geo.material.Vacuum(),
                              sensitive=False,
                              color=None,
                              parent=None,
                              translation=None,
                              rotation=None)

# Get prebuilt MINER

minerXL = g4py.geo.prebuilt.minerXL()

# Place in world
minerXL.placeit(parent=world,
             translation=None,
             rotation=[np.deg2rad(0),np.deg2rad(0)],
             rotationName='rotAx')

'''
PuBeSource = g4py.geo.prebuilt.beSphere("PuBe", copies=1)
PuBeSource.placeit(parent=world,
             translation=[0,0,3000],
             rotation=None,
             rotationName=None)
'''
# Setup source
'''
python scripts/miner.py -n 50000000 -E 2000 -healpix 16 0 -mp
'''
ang = []
for i in range(19):
    dir = [5*i,0]
    ang += [dir]
# print ang
source = g4py.Source(field='isotropicPos',
                     particle='neutron',
                     energy=6,#[0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10],
                     energy_unit='MeV',
                     direction=ang, #should be nside if direction_type is healpix
                     direction_type='angle',
                     num_particles=int(1e8),
                     source_rad=5,
                     source_dist=300,
                     length_unit='cm',
                     random_seed=13,
                     outputfile='output/minerxlAngularRes1e8')

# Send to sim
run = g4py.Run(world=world,
               source=source,
               check_overlaps=True,
               track_secondaries=True,
               vis_driver=None,#'oglsx',
               physics_list='QGSP_BERT_HP',
               process_tracking = ['ioni', 'elastic'])
