import geant4py as g4py
import numpy as np
import matplotlib.pyplot as plt
import Geant4 as g4
from Geant4 import gRunManager, HepRandom, G4String, G4physicslists, G4UImanager
from Geant4 import *


# Get prebuilt world volume
'''
world = g4py.geo.prebuilt.world(primitive='sphere',
                                material=g4py.geo.material.Vacuum())
'''
world_primitive = g4py.geo.primitive.Sphere('world', dim={'inner_rad':[0,'m'],
                                                 'outer_rad':[50, 'm'],
                                                 'sPhi':[0,'deg'],
                                                 'dPhi':[360,'deg'],
                                                 'sTheta':[0,'deg'],
                                                 'dTheta':[180,'deg']})
world = g4py.geo.volume.Volume(name='world',
                              primitive=world_primitive,
                              material=g4py.geo.material.Vacuum(),
                              sensitive=False,
                              color=None,
                              parent=None,
                              translation=None,
                              rotation=None)
# Get prebuilt MINER

porcupine = g4py.geo.prebuilt.porcupineSingle()
# print porcupine

# Place in world

porcupine.placeit(parent=world,
             translation=None,
             rotation=None,#[np.deg2rad(90),0],
             rotationName='rotAx')
'''
PuBeSource = g4py.geo.prebuilt.beSphere("PuBe", copies=1)
PuBeSource.placeit(parent=world,
             translation=[0,0,3000],
             rotation=None,
             rotationName=None)
'''
# Setup source
'''
python scripts/miner.py -n 5000000 -E 2000 -healpix 16 0 -mp
'''
elist = [0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10]
source = g4py.Source(field='far',
                     particle='neutron',
                     energy=[0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10],#4.6,
                     energy_unit='MeV',
                     direction=[0,0], #should be nside if direction_type is healpix
                     direction_type='angle',
                     num_particles=int(5e4),
                     source_rad=6.85,
                     source_dist=500,
                     length_unit='cm',
                     random_seed=42,
                     outputfile='output/porcupineAlCols5e4Particles')

'''
mat = g4py.geo.material.B4C()
print(mat)
g4.G4VCrossSectionHandler.PrintData#(g4.G4ProcessType.fHadronic))
xsection = g4.float(G4VCrossSectionHandler.ValueForMaterial)#(5e6, "neutron",'hadelastic',
                                            # 'polyethylene') * g4.cm2/g4.g
# print xsection

plt.figure()
plt.plot(elist,xsection['tot'])
plt.show()
neutronxSection = []
print(g4.gRunManager)
physfactory = G4physicslists.G4PhysListFactory()
physlist = physfactory.GetReferencePhysList('QGSP_BERT_HP')
print(g4.gEmCalculator.ComputeCrossSectionPerVolume((elist[0], "gamma", "tot",
                                                   g4py.geo.material.SWX210()) * cm2/g))
'''
# Send to sim
run = g4py.Run(world=world,
               source=source,
               check_overlaps=True,
               track_secondaries=True,
               vis_driver=None,#'oglsx',
               physics_list='QGSP_BERT_HP')#"FTFP_BERT_LIV")
