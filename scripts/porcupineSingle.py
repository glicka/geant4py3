import geant4py as g4py
import numpy as np
import matplotlib.pyplot as plt
import Geant4 as g4
from Geant4 import gRunManager, HepRandom, G4String, G4physicslists, G4UImanager
from Geant4 import *
import healpy as hp

phiRot = np.linspace(0,90,36)
ang = []
for i in range(len(phiRot)):
    ang += [[phiRot[i],0.]]
# print len(ang)


'''
nside = 8
[theta, phi] = hp.pix2ang(nside,range(12*nside**2))
ang = []
for i in range(len(theta)):
    if np.rad2deg(theta[i]) <= 90.:
        ang += [[np.rad2deg(theta[i]),np.rad2deg(phi[i])]]

# print len(ang)
'''
# Get prebuilt world volume
'''
detH = 63.5*2 + thickness*2 + window
pmtH = 139.7 + 25.4 + 63.5*2
(25.4*14 + 1)
thickness = 5. #mm
window = 0.65 #mm
'''
'''
world = g4py.geo.prebuilt.world(primitive='sphere',
                                material=g4py.geo.material.Vacuum())
'''
world_primitive = g4py.geo.primitive.Sphere('world', dim={'inner_rad':[0,'m'],
                                                 'outer_rad':[50, 'm'],
                                                 'sPhi':[0,'deg'],
                                                 'dPhi':[360,'deg'],
                                                 'sTheta':[0,'deg'],
                                                 'dTheta':[180,'deg']})
world = g4py.geo.volume.Volume(name='world',
                              primitive=world_primitive,
                              material=g4py.geo.material.Vacuum(),
                              sensitive=False,
                              color=None,
                              parent=None,
                              translation=None,
                              rotation=None)
# Get prebuilt MINER

porcupine = g4py.geo.prebuilt.porcupineSingle()
# print porcupine

# Place in world

porcupine.placeit(parent=world,
             translation=None,
             rotation=[np.deg2rad(0),np.deg2rad(0)],
             rotationName='rotAx')

# Setup source
'''
python scripts/miner.py -n 5000000 -E 2000 -healpix 16 0 -mp
'''

source = g4py.Source(field='near',
                     particle='neutron',
                     energy=3,
                     minE=0.35,
                     maxE=15,
                     energy_unit='MeV',
                     direction=ang, #should be nside if direction_type is healpix
                     direction_type='angle',
                     num_particles=int(1e6),
                     source_rad=64.77,
                     source_dist=1000,
                     length_unit='cm',
                     random_seed=13,
                     outputfile='output/porcupine1Col14inAll')


# Send to sim
run = g4py.Run(world=world,
               source=source,
               check_overlaps=True,
               track_secondaries=True,
               vis_driver=None,#'oglsx',
               physics_list='QGSP_BERT_HP',
               process_tracking = ['ioni', 'elastic'])
