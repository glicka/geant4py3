import geant4py as g4py
import numpy as np
import healpy as hp

nside = 30
[theta, phi] = hp.pix2ang(nside,range(12*nside**2))
ang = []
for i in range(len(theta)):
    if np.rad2deg(theta[i]) <= 90.:
        ang += [[np.rad2deg(theta[i]),np.rad2deg(phi[i])]]


# Get prebuilt world volume
'''
world = g4py.geo.prebuilt.world(primitive='sphere',
                                material=g4py.geo.material.Vacuum())
'''
world_primitive = g4py.geo.primitive.Sphere('world', dim={'inner_rad':[0,'m'],
                                                 'outer_rad':[50, 'm'],
                                                 'sPhi':[0,'deg'],
                                                 'dPhi':[360,'deg'],
                                                 'sTheta':[0,'deg'],
                                                 'dTheta':[180,'deg']})
world = g4py.geo.volume.Volume(name='world',
                              primitive=world_primitive,
                              material=g4py.geo.material.Air(),
                              sensitive=False,
                              color=None,
                              parent=None,
                              translation=None,
                              rotation=None)
# Get prebuilt MINER

porcupine = g4py.geo.prebuilt.porcupineFrameWConcreteFloor()


# Place in world
porcupine.placeit(parent=world,
             translation=None,
             rotation=[np.deg2rad(0),np.deg2rad(0)],
             rotationName='rotAx')

# Setup source
'''
ang = []
for i in range(36):
    if i > 0:
        for j in range(144):
            dir = [2.5*i,2.5*j]
            ang += [dir]
    else:
        dir = [2.5*i,0]
        ang += [dir]
# print ang
'''


'''
python scripts/miner.py -n 5000000 -E 5000 -healpix 16 0 -mp
'''

source = g4py.Source(field='near',
                     particle='neutron',
                     energy=3,
                     minE=0.5,
                     maxE=15,
                     energy_unit='MeV',
                     direction=ang, #should be nside if direction_type is healpix
                     direction_type='angle',
                     num_particles=int(1.5e5),
                     source_rad=200,
                     source_dist=4000,
                     length_unit='cm',
                     random_seed=46,
                     outputfile='/global/scratch/adam_glick/output/linisConcreteE46',
                     sourcedatafile='/global/scratch/adam_glick/srcdata/linisConcreteE46')

# Send to sim
run = g4py.Run(world=world,
               source=source,
               check_overlaps=True,
               track_secondaries=True,
               vis_driver=None,#'oglsx',
               physics_list='QGSP_BERT_HP',
               process_tracking = ['ioni', 'elastic'])
