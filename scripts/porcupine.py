import geant4py as g4py
import numpy as np

# Get prebuilt world volume
'''
world = g4py.geo.prebuilt.world(primitive='sphere',
                                material=g4py.geo.material.Vacuum())
'''
world_primitive = g4py.geo.primitive.Sphere('world', dim={'inner_rad':[0,'m'],
                                                 'outer_rad':[50, 'm'],
                                                 'sPhi':[0,'deg'],
                                                 'dPhi':[360,'deg'],
                                                 'sTheta':[0,'deg'],
                                                 'dTheta':[180,'deg']})
world = g4py.geo.volume.Volume(name='world',
                              primitive=world_primitive,
                              material=g4py.geo.material.Vacuum(),
                              sensitive=False,
                              color=None,
                              parent=None,
                              translation=None,
                              rotation=None)
# Get prebuilt MINER

porcupine = g4py.geo.prebuilt.porcupine()
# print porcupine

'''
no concrete = 11.52 seconds
stack len 10 = 88.81 seconds
stack len 50 = 85.56 seconds
stack len 500 = 93.12 seconds
no stacking len = 83.37 seconds
'''

# Place in world
porcupine.placeit(parent=world,
             translation=None,
             rotation=[np.deg2rad(0),np.deg2rad(0)],
             rotationName='rotAx')

# Setup source
'''
python scripts/miner.py -n 5000000 -E 2000 -healpix 16 0 -mp
'''
source = g4py.Source(field='near',
                     particle='neutron',
                     energy=3,#[0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10],
                     minE=0.5,
                     maxE=15,
                     energy_unit='MeV',
                     direction=[45,0], #should be nside if direction_type is healpix
                     direction_type='angle',
                     num_particles=int(1e4),
                     source_rad=187,
                     source_dist=500,
                     length_unit='cm',
                     random_seed=13,
                     outputfile='output/test')

# Send to sim
run = g4py.Run(world=world,
               source=source,
               check_overlaps=True,
               track_secondaries=True,
               vis_driver=None,#'oglsx',
               physics_list='QGSP_BERT_HP',
               process_tracking = ['ioni', 'elastic'])
