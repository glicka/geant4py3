import geant4py as g4py
import numpy as np
import healpy as hp


nside = 8
[theta, phi] = hp.pix2ang(nside,range(12*nside**2))
ang = []
for i in range(len(theta)):
    ang += [[np.rad2deg(theta[i]),np.rad2deg(phi[i])]]


# Get prebuilt world volume
world_primitive = g4py.geo.primitive.Sphere('world', dim={'inner_rad':[0,'m'],
                                                 'outer_rad':[10, 'm'],
                                                 'sPhi':[0,'deg'],
                                                 'dPhi':[360,'deg'],
                                                 'sTheta':[0,'deg'],
                                                 'dTheta':[180,'deg']})
world = g4py.geo.volume.Volume(name='world',
                              primitive=world_primitive,
                              material=g4py.geo.material.Vacuum(),
                              sensitive=False,
                              color=None,
                              parent=None,
                              translation=None,
                              rotation=None)
# Get prebuilt MINER

miner = g4py.geo.prebuilt.miner()

# Place in world
miner.placeit(parent=world,
             translation=None,
             rotation=[np.deg2rad(0),np.deg2rad(0)],
             rotationName='rotAx')

# Setup source
'''
python scripts/miner.py -n 50000000 -E 2000 -healpix 16 0 -mp
'''

source = g4py.Source(field='near',
                     particle='neutron',
                     energy=3,#[0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5, 5.5, 6, 6.5, 7, 7.5, 8, 8.5, 9, 9.5, 10],
                     minE=0.5,
                     maxE=15,
                     energy_unit='MeV',
                     direction=[0,0],
                     direction_type='angle',
                     num_particles=int(1),
                     source_rad=35,
                     source_dist=300,
                     length_unit='cm',
                     random_seed=5,
                     outputfile='output/minerSimE6',
                     sourcedatafile='srcdata/minerSimE6')

# Send to sim
run = g4py.Run(world=world,
               source=source,
               check_overlaps=True,
               track_secondaries=True,
               vis_driver='oglsx',
               physics_list=['QGSP_BERT_HP'],
               process_tracking = ['ioni', 'elastic'])#"FTFP_BERT_LIV")QGSP_BERT_HP
